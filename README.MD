# TASK MANAGER

## DEVELOPER INFO

* **Name**: Evgeniy Oskin

* **E-mail**: jizer@inbox.ru

## SOFTWARE

* **OS**: Windows 10 Корпоративная LTSC

* **Java**: OpenJDK version "1.8.0_345"

## HARDWARE

* **CPU**: i5-8250U

* **RAM**: 16 Gb

* **SSD**: 512 Gb

## PROGRAM BUILD

```shell
mvn clean install
```

## PROGRAM RUN

```shell
java -jar task-manager.jar
```